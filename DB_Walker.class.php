<?php

class DB {
	protected $host;
	protected $user;
	protected $pass;
	protected $base;
	public $link;
	public $params;

	function __construct() {
		global $localhost;
		global $debug;
		$this->debug = $debug;

		$mysql_access_file = "mysql_access";

		$mysql_access_file = file_get_contents($mysql_access_file);
		$access_rows = explode("\n", $mysql_access_file);
		$mysql_access = [];
		foreach($access_rows as $row):
			if(!empty($row)):
				$fields = explode(":", $row);
				$mysql_access[] = trim($fields[1]);
			endif;
		endforeach;
		list($this->host, $this->user, $this->pass, $this->base) = $mysql_access;

		// Conecta ao banco de dados
		$this->link = new mysqli($this->host, $this->user, $this->pass, $this->base);
		$this->link->set_charset('utf8');
	}

	function __destruct() {
		if(isset($this->link)):
			mysqli_close($this->link);
			$this->link = false;
			return true;
		endif;
	}

	public function isAssoc(array $arr) {
		if (array() === $arr) return false;

		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	public function query($query) {
		$result = $this->link->query($this->link->escape_string(trim($query)));
		return $result;
	}

	///////////////////////////////////////////////////////////////////////////////////
	///  SELECT ///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	public function select($options, $debug = false) {
		// GET options
		$query_table = $options["table"];
		$columns = (isset($options["columns"])) ? $options["columns"] : null;
		$joins = (isset($options["joins"])) ? $options["joins"] : null;
		$where = (isset($options["where"])) ? $options["where"] : null;
		$order = (isset($options["order"])) ? $options["order"] : null;
		$limit = (isset($options["limit"])) ? $options["limit"] : null;
		$offset = (isset($options["offset"])) ? $options["offset"] : ((isset($options["limit"])) ? 0 : NULL);

		if(is_array($columns)) $columns = implode(", ", $columns);

		if(is_null($where)):
			$query_where = NULL;
		elseif(is_string($where)):
			$query_where = " WHERE ".$where;
		else:
			if($this->isAssoc($where)):
				$params = array();
				foreach ($where as $param => $value):
					if((!is_int($value) || !is_float($value)) && strpos($value, "('") === false && $value !== "NULL")
						$value = "'".trim($value)."'";

					$params[] = "{$param} = {$value}";
				endforeach;

				$where = $params;
			endif;

			$query_where = " WHERE ".implode(" AND ", $where);
		endif;

		if(is_null($joins)):
			$query_joins = NULL;
		elseif(is_string($joins)):
			$query_joins = " INNER JOIN ".$joins;
		else:
			$joins_arr = [];
			foreach ($joins as $join):
				$join_table = $join["table"];
				$join_direction = strtoupper($join["direction"]);
				$join_on = (is_string($join["on"])) ? $join["on"] : implode(" AND ", $join["on"]);

				$joins_arr[] = " {$join_direction} JOIN {$join_table} ON ({$join_on})";
			endforeach;

			$query_joins = implode(" ", $joins_arr);
		endif;

		$query_columns = (!is_null($columns)) ? $columns : $query_columns = "*";
		$query_order = (!is_null($order)) ? " ORDER BY ".$order : NULL;
		$query_limit = (!is_null($limit) && 0 < $limit) ? " LIMIT ".$limit : NULL;
		$query_offset = (!is_null($limit) && !is_null($offset) && 0 <= $offset) ? " OFFSET ".$offset : NULL;


		$query = "SELECT {$query_columns} FROM {$query_table}{$query_joins}{$query_where}{$query_order}{$query_limit}{$query_offset}";

		$response = [];
		$response["success"] = false;
		if($this->debug || $debug) $response["options"] = $options;
		if($this->debug || $debug) $response["query"] = $query;

		if (!$result = $this->link->query($query)):
			$response["message"] = $this->link->error;
		else:
			$response["success"] = true;
			$response["results"] = $result->num_rows;
			$response["found"] = $result->num_rows;

			if(!is_null($limit) && 0 < $result->num_rows):
				$table_name = array_reverse(explode(" ", $query_table))[0];

				$query_total = "SELECT COUNT({$table_name}.id) AS total FROM {$query_table}{$query_joins}{$query_where}";
				if(!$result_total = $this->link->query($query_total)):
					$response["message_total"] = $this->link->error;
				else:
					$result_total = $result_total->fetch_object();
					$response["total"] = (int) $result_total->total;
					$response["limit"] = (int) $limit;
				endif;
			endif;

			$response["data"] = [];
			while ($row = $result->fetch_object()):
				$response["data"][] = $row;
			endwhile;
		endif;

		return $response;
	}

	///////////////////////////////////////////////////////////////////////////////////
	///  INSERT  //////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	public function insert($options, $debug = false) {
		$query_table = $options["table"];
		$data = $options["data"];

		foreach ($data as $key => $value) {
			if((!is_int($value) || !is_float($value)) && strpos($value, "('") === false && $value !== "NULL")
				$value = "'".$this->link->escape_string(trim($value))."'";

			$cols[] = $key;
			$values[] = $value;
		}

		$query_cols = implode(", ", $cols);
		$query_values = implode(", ", $values);
		$query = "INSERT INTO $query_table ($query_cols) VALUES ($query_values)";

		$response = [];
		$response["success"] = false;
		if($this->debug || $debug) $response["options"] = $options;
		if($this->debug || $debug) $response["query"] = $query;

		if(!$insert = $this->link->query($query)):
			$response["message"] = $this->link->error;
		else:
			$id = $this->link->insert_id;
			$response["success"] = true;
			$response["insert_id"] = $id;

			$result = $this->select(["table"=>$query_table,"where"=>["{$query_table}.id"=>$id]]);
			if($result["success"]):
				$response["data"] = $result["data"][0];
			endif;
		endif;

		return $response;
	}

	///////////////////////////////////////////////////////////////////////////////////
	///  UPDATE ///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	public function update($options, $debug = false) {
		// GET options
		$query_table = $options["table"];
		$joins = (isset($options["joins"])) ? $options["joins"] : null;
		$where = (isset($options["where"])) ? $options["where"] : null;
		$data = (isset($options["data"])) ? $options["data"] : null;

		if(is_null($where)):
			$query_where = NULL;
		elseif(is_string($where)):
			$query_where = " WHERE ".$where;
		else:
			if($this->isAssoc($where)):
				$params = array();
				foreach ($where as $param => $value):
					if((!is_int($value) || !is_float($value)) && strpos($value, "('") === false && $value !== "NULL")
						$value = "'".trim($value)."'";

					$params[] = "{$param} = {$value}";
				endforeach;

				$where = $params;
			endif;

			$query_where = " WHERE ".implode(" AND ", $where);
		endif;

		foreach ($data as $key => $value):
			if((!is_int($value) || !is_float($value)) && strpos($value, "('") === false && $value !== "NULL")
				$value = "'".$this->link->escape_string(trim($value))."'";

			$query_fields[] = "{$key} = {$value}";
		endforeach;

		$query_fields = " SET ".implode(", ", $query_fields);

		$query = "UPDATE {$query_table}{$query_fields}{$query_where}";

		$response = [];
		$response["success"] = false;
		if($this->debug || $debug) $response["options"] = $options;
		if($this->debug || $debug) $response["query"] = $query;

		if (!$result = $this->link->query($query)):
			$response["message"] = $this->link->error;
		else:
			$response["success"] = true;
		endif;

		return $response;
	}

	//////////////////////////////////////////////////////////////////////////////////
	///  DELETE //////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	public function delete($table, $dados) {

	}
}
?>